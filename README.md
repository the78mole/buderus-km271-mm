# README #

This repository is an example for a firmware running on the KM271 hardware described in MolesBlog here:
[Reverse Engineering the Buderus KM271 � And Making It WiFi-Flying on ESPhome and Home Assistant](https://the78mole.de/reverse-engineering-the-buderus-km217/)

The KM271 is a little hardware to be plugged into a Buderus Logamatic Heating Control which allows to log the operating values of the heating control.

### What is this repository for? ###

* This firmware is running as an ESP32 Arduino IDE project
* It does not use ESPHome or any other environment. In fact, it was written as a plug-in replacement for an existing, very proprietray home automation environment.
* As such, it is probably of less interest to people wanting a plug-and-play solution.
* Instead, it can be seen as a useful example how to build a firmware for a KM271 module from scratch.
* Especially, the km271_prot.cpp source code might be of interest, as it handles the KM271 3964 protocol and translates the received information into a generic structure containing many of the operating parameters of the heating.
* The webinterface.cpp is quite proprietary and shows how to retrieve the information from the web and how to push the information into a server based database.

### How do I get set up? ###

* Install Arduino-IDE
* Install the ESP32-environment for the Arduino-IDE
* Use the library administration to install the additional component "TelnetStream"

### Some features ###

* Extensive logging can be configured
* Logging via serial output or remotely via telnet stream
* Using telnet you can issue a reset or retrieve some firmware status information  
* OTA update of the firmware possible

### Limitations ###

* The hardware as described in MolesBlog was not running stable in my environment.
* From time to time, there were brown-out errors recognized by the ESP32 which result in sporadic module resets
* The reason is the very weak 5V power supply from the Buderus Logatamtic to the KM271. This 5V supply was not intended to drive a ESP32.
When the ESP32 needs a lot of power (e.g. when registering to the Wifi network), the 5V supply drops down in voltage and then even may lead to brown-out at ESP32.
* More capacity on 5V input or 3.3V supply of the ESP32 do not really help to stablelize the power supply. The drop outs lasts too long and the voltage drops too low.
* My solution:
I put a separated power supply for the ESP32. The 5V from the Buderus Logamatic were only be used for level shifting of the signals, not for supplying the ESP32 any more. 
From that time on, everything was stable.
An alternative might be to use a low drop regulator. E.eg. an LP38501. But I haven't tested that.

### Disclaimer ###

* Again, this firmware is to be seen as just an example.
* Sorry for the coding, I am not an expert in Arduino, nor in ESP32 nor in CPP. So certainly, this can be done better!
* Use it at your own risk...
