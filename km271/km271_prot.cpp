//*****************************************************************************
//
// File Name  : 'km271_prot.cpp'
// Title      : Handles 3964 protocol for KM271
// Author     : Michael Meyer
// Created    : 08.01.2022
// Version    : 0.1
// Target MCU : ESP32/Arduino
// Indicator  : km
//
//
//*****************************************************************************

/** I N C L U D E S **********************************************************/
#include "Arduino.h"
#include "km271_prot.h"

// Telnet Stream for logging / debugging over Wifi
#include <TelnetStream.h>

/* D E C L A R A T I O N S **************************************************/
// Configuration
#define KM271_EN_PROTLOG          0                                       // Enable/disable protocol logging (most of protocol bytes are reported, but DLE doubling is missing in RX!)
#define KM271_EN_PARSELOG         0                                       // Enable/disable parsing logging (only blocks to be parsed are reported)
#define KM271_EN_PARSE_RESULTLOG  1                                       // Enable/disable parsing result logging: Clear text logging.


// Protocol elements. Do not change, otherwise KM271 communication will fail!
#define KM271_BAUDRATE        2400                                        // The baudrate top be used for KM271 communication
#define KM_STX                0x02                                        // Protocol control bytes
#define KM_DLE                0x10
#define KM_ETX                0x03
#define KM_NAK                0x15

#define KM_RX_BUF_LEN         16                                          // Max number of RX bytes (8 pure data bytes shoud be sufficient)
#define KM_TX_BUF_LEN         16                                          // Max number of TX bytes (8 pure data bytes shoud be sufficient)

// The states to receive a single block of data.
// First a block iof data is received byte by byte by using this state interpreter.
// If a full block of data is received, a second level state interopreter is called to handle the blocks.
typedef enum {
  KM_RX_RESYNC,                                                           // Unknown state, re-sync by wait for STX
  KM_RX_IDLE,                                                             // Idle state for RX interrupt routine
  KM_RX_ON,                                                               // Block reception started
  KM_RX_DLE,                                                              // DLE doubling
  KM_RX_ETX,                                                              // End detected
  KM_RX_BCC,                                                              // Verify block
} e_rxState;

// The higher level states used in handleRxBlock();
typedef enum {
  KM_TSK_START,                                                           // Switch to logging mode
  KM_TSK_LG_CMD,                                                          // Receive confirmation from KM
  KM_TSK_LOGGING,                                                         // Logging active
} e_rxBlockState; 

typedef struct {                                                          // Rx structure for one rx block
  uint8_t                   len;                                          // Length of data in buffer
  uint8_t                   buf[KM_RX_BUF_LEN];                           // Received bytes without "10 03 bcc"
} KmRx_s;

/* V A R I A B L E S ********************************************************/

TaskHandle_t                hKmRxTaskHandle;                              // To store tge handle of the RX task
static SemaphoreHandle_t    accessMutex;                                  // To protect access to kmState structure

// This structure contains the complete received status of the KM271 (as far it is parsed by now).
// It is updated automatically on any changes by this driver and therefore kept up-to-date.
// Do not access it directly from outside this source to avoid inconsistency of the structure.
// Instead, use km271GetStatus() to get a current copy in a safe manner.
static s_km271_status       kmState;                                      // All current KM271 values  

// Status machine handling
static e_rxBlockState       KmRxBlockState;                               // The RX block state
static QueueHandle_t        eventQueue;                                   // To generate events for web interface

// known commands to KM271, to be used with KmStartTx()
static uint8_t KmCSTX[]     = {KM_STX};                                   // STX Command
static uint8_t KmCDLE[]     = {KM_DLE};                                   // DLE Response
static uint8_t KmCNAK[]     = {KM_NAK};                                   // NAK Response
static uint8_t KmCLogMode[] = {0xEE, 0x00, 0x00};                         // Switch to Log Mode

// Unused commands
// This is for writing to the KM271. Not supported by this driver, yet 
// static uint8_t KmCWWNight[] = {0x0C, 0x0E, 0x00, 0x65, 0x65, 0x65, 0x65, 0x65};
// static uint8_t KmCWWDay[]   = {0x0C, 0x0E, 0x01, 0x65, 0x65, 0x65, 0x65, 0x65};
// static uint8_t KmCWWAuto[]  = {0x0C, 0x0E, 0x02, 0x65, 0x65, 0x65, 0x65, 0x65};

/* Local F U N C T T I O N  P R T O T Y P E S *******************************/
static void myLog(const char *txt);
static void myLogLn(const char *txt);
static void KmRxTask(void * parameter);
static void sendTxBlock(uint8_t *data, int len);
static void handleRxBlock(uint8_t *data, int len, uint8_t bcc);
static void parseInfo(uint8_t *data, int len);
static float decode05cTemp(uint8_t data);
static float decodeNegTemp(uint8_t data);
static uint32_t buildEventList(s_km271_status *pState);

/* C O D E ******************************************************************/

/**
 *  ******************************************************
 * @brief   Initializes the KM271 protocoll (based on 3964 protocol)
 * @details 
 * @param   rxPin   The ESP32 RX pin number to be used for KM271 communication
 * @param   txPin   The ESP32 TX pin number to be used for KM271 communication
 * @param   eventQ  A handle of a queue which is used to send events to. If set to NULL, no events are generated.
 * @return  e_ret error code
 */
e_ret km271ProtInit(int rxPin, int txPin, QueueHandle_t eventQ) {
  const char* init = "Initializing KM271 communication interface";
  myLogLn("Initializing KM271 communication interface");
  Serial2.begin(KM271_BAUDRATE, SERIAL_8N1, rxPin, txPin);                // Set serial port for communication with KM271/Ecomatic 2000

  // Store received event queue handle.
  // When events are parsed, the event is sent into this queue (if != NULL).
  eventQueue = eventQ;

  // Create the mutex to access the kmState structure in a safe manner
  accessMutex = xSemaphoreCreateMutex();

  // Create additional task for handling the UART communication protocoll based on KM271.
  // This is the RX task basically. All communication is driven by the Ecomatic 2000 is the initiator.
  xTaskCreatePinnedToCore(
    KmRxTask,       // Function that should be called
    "KMRxTask",     // Name of the task (for debugging)
    5000,           // Stack size (bytes)
    NULL,           // Parameter to pass
    1,              // Task priority
    &hKmRxTaskHandle, // Task handle
    1               // Use core 1 always (same as loop() is using. Do not interfere with WLAN, which runs on core 0.
  );

  return RET_OK;  
}

/**
 *  ******************************************************
 * @brief   Retrieves the current status and copies it into
 *          the destination given.
 * @details This is under task lock to ensure consistency of status structure.
 * @param   pDestStatus: The destination address the status shall be stored to
 * @return  none
 */
void km271GetStatus(s_km271_status *pDestStatus) {
  if(accessMutex) {                                                       // Just in case the mutex is not initialzed when another task tries to use it
    xSemaphoreTake(accessMutex, portMAX_DELAY);                           // Prevent task switch to ensure the whole structure remains constistent
    memcpy(pDestStatus, &kmState, sizeof(s_km271_status));
    xSemaphoreGive(accessMutex);                                          // We may continue normally here
  }
}

/**
 *  ******************************************************
 * @brief   Reset max values track in status structure
 * @details Can be called to reset certain values. E.g. every midnight
 *          to reset values that are tracked daily.
 * @return  none
 */
void km271ResetDailyValues() {
  if(accessMutex) {                                                       // Just in case the mutex is not initialzed when another task tries to use it
    xSemaphoreTake(accessMutex, portMAX_DELAY);                           // Prevent task switch to ensure the whole structure remains constistent
    kmState.MaxExhaustTemp = kmState.ExhaustTemp;                         // Reset the maximum exhaust temperature to the current exhaust temperature
    xSemaphoreGive(accessMutex);                                          // We may continue normally here
  }
}

/**
 *  ******************************************************
 * @brief   The Rx task for receiving bytes from the KM271
 * @details Single bytes are read from the KM271 UART interface and handled according
 *          to the 3964 protocol definition.
 *          Error handling and BCC calculation is done.
 *          Successfully received blocks are provided to the handleRxBlock() function. Data is 
 *          provided as "clean2 data, i.e. DLE doubling is considered, only payload 
 *          is given without DLE,ETX,BCC
 * @param   parameter   Optional pointer to task parameters (unused)
 * @return  none
 */
static void KmRxTask(void * parameter){
  uint8_t     rxByte;                                                     // The received character. We are reading byte by byte only.
  e_rxState   kmRxStatus;                                                 // Status in Rx reception
  uint8_t     kmRxBcc;                                                    // BCC value for Rx Block
  uint8_t     kmRxBufIndex;                                               // Index of current RxBuf (0 or one)
  KmRx_s      kmRxBuf;                                                    // Rx block storage
  
  KmRxBlockState = KM_TSK_START;                                          // Initialize the block state
  kmRxStatus = KM_RX_RESYNC;                                              // Make sure to re-sync first
  kmRxBcc = 0;                                                            // Init BCC

  // Reset maximum values tracked by KM271 module
  km271ResetDailyValues();

  myLogLn("Initializing KM271 successful!");
  myLogLn("Waiting for KM271 data now...");
  
  for(;;){                                                                // Infinite loop
    vTaskDelay(8 / portTICK_PERIOD_MS);                                   // Just for the case that there is a looooong list of RX 
                                                                          // data faster received than executed and therefore watchdog may be trigger.
                                                                          // Very unlikely to happen, but slower is less harmfull than watchdog...
    if(Serial2.readBytes(&rxByte, 1)) {                                   // Wait for RX byte, if timeout, just loop and read again
      // Protocol handling
      kmRxBcc ^= rxByte;                                                  // Calculate BCC
      switch(kmRxStatus) {
        case KM_RX_RESYNC:                                                // Unknown state, discard everthing but STX
          if(rxByte == KM_STX) {                                          // React on STX only to re-synchronise
            kmRxBuf.buf[0] = KM_STX;                                      // Store current STX
            kmRxBuf.len = 1;                                              // Set length
            kmRxStatus = KM_RX_IDLE;                                      // Sync done, now continue to receive
            handleRxBlock(kmRxBuf.buf, kmRxBuf.len, rxByte);              // Handle RX block
          }
          break;
        case KM_RX_IDLE:                                                  // Start of block or command
          kmRxBuf.buf[0] = rxByte;                                        // Store current byte
          kmRxBuf.len = 1;                                                // Initialise length
          kmRxBcc = rxByte;                                               // Reset BCC
          if((rxByte == KM_STX) || (rxByte == KM_DLE) || (rxByte == KM_NAK)) {    // Give STX, DLE, NAK directly to caller
            handleRxBlock(kmRxBuf.buf, kmRxBuf.len, rxByte);              // Handle RX block
          } else {                                                        // Whole block will follow
            kmRxStatus = KM_RX_ON;                                        // More data to follow, start collecting
          }
          break;                      
        case KM_RX_ON:                                                    // Block reception ongoing
          if(rxByte == KM_DLE) {                                          // Handle DLE doubling
            kmRxStatus = KM_RX_DLE;                                       // Discard first received DLE, could be doubling or end of block, check in next state
            break;                                                        // Quit here without storing
          }
          if(kmRxBuf.len >= KM_RX_BUF_LEN) {                              // Check allowed block len, if too long, re-sync
            kmRxStatus = KM_RX_RESYNC;                                    // Enter re-sync
            break;                                                        // Do not save data beyond array border
          }
          kmRxBuf.buf[kmRxBuf.len] = rxByte;                              // No DLE -> store regular, current byte
          kmRxBuf.len++;                                                  // Adjust length in rx buffer
          break;
        case KM_RX_DLE:                                                   // Entered when one DLE was already received
          if(rxByte == KM_DLE) {                                          // Double DLE?
            if(kmRxBuf.len >= KM_RX_BUF_LEN) {                            // Check allowed block len, if too long, re-sync
              kmRxStatus = KM_RX_RESYNC;                                  // Enter re-sync
              break;                                                      // Do not save data beyond array border
            }
            kmRxBuf.buf[kmRxBuf.len] = rxByte;                            // Yes -> store this DLE as valid part of data
            kmRxBuf.len++;                                                // Adjust length in rx buffer
            kmRxStatus = KM_RX_ON;                                        // Continue to receive block
          } else {                                                        // This should be ETX now
            if(rxByte == KM_ETX) {                                        // Really? then we are done, just waiting for BCC
              kmRxStatus = KM_RX_BCC;                                     // Receive BCC and verify it
            } else {
              kmRxStatus = KM_RX_RESYNC;                                  // Something wrong, just try to restart 
            }
          }
          break;
        case KM_RX_BCC:                                                   // Last stage, BCC verification, "received BCC" ^ "calculated BCC" shall be 0 
          if(!kmRxBcc) {                                                  // Block is valid
            handleRxBlock(kmRxBuf.buf, kmRxBuf.len, rxByte);              // Handle RX block, provide BCC for debug logging, too
          } else {
            sendTxBlock(KmCNAK, sizeof(KmCNAK));                          // Send NAK, ask for re-sending the block
          }
          kmRxStatus = KM_RX_IDLE;                                        // Wait for next data or re-sent block
          break;
      }
    }  
  }
}

/**
 *  ******************************************************
 * @brief   Handling of a whole RX block received by the RX task.
 * @details A whole block of RX data is processed according to the current
 *          operating state.
 *          The operating state ensures, that we enable the logging feature of the
 *          KM271. During logging, the KM271 constantlöy updates all information
 *          which has changed automatically.
 * @param   data: Pointer to the block of data received.
 * @param   len:  Blocksize in number of bytes, without protocol data 
 * @param   bcc:  The BCC byte for a regular data block. Only valid for data blocks. Used for debugging only
 * @return  none
 */
static void handleRxBlock(uint8_t *data, int len, uint8_t bcc) {
  int           ii;
  #if (KM271_EN_PROTLOG == 1)
  char          outBuf[50];                                                 // Just for debugging
  #endif

  switch(KmRxBlockState) {
    case KM_TSK_START:                                                      // We need to switch to logging mode, first
      switch(data[0]) {
        case KM_STX:                                                        // First step: wait for STX
          #if (KM271_EN_PROTLOG == 1)
          myLogLn("RX: STX");
          #endif
          sendTxBlock(KmCSTX, sizeof(KmCSTX));                              // Send STX to KM271
          break;
        case KM_DLE:                                                        // DLE received, KM ready to receive command
          #if (KM271_EN_PROTLOG == 1)
          myLogLn("RX: DLE");
          #endif
          sendTxBlock(KmCLogMode, sizeof(KmCLogMode));                      // Send logging command
          KmRxBlockState = KM_TSK_LG_CMD;                                   // Switch to check for logging mode state
          break;
      }
      break;
    case KM_TSK_LG_CMD:                                                     // Check if logging mode is accepted by KM
      if(data[0] != KM_DLE) {                                               // No DLE, not accepted, try again
        #if (KM271_EN_PROTLOG == 1)
        myLogLn("RX: DLE");
        #endif
        KmRxBlockState = KM_TSK_START;                                      // Back to START state
      } else {
        #if (KM271_EN_PROTLOG == 1)
        sprintf(outBuf, "RX: %02X ", data[0]);
        myLogLn(outBuf);
        #endif
        KmRxBlockState = KM_TSK_LOGGING;                                    // Command accepted, ready to log!
      }
      break;
    case KM_TSK_LOGGING:                                                    // We have reached logging state
      if(data[0] == KM_STX) {                                               // If STX, this is a send request, to be confirmed
        #if (KM271_EN_PROTLOG == 1)
        myLogLn("RX: STX");
        #endif
        sendTxBlock(KmCDLE, sizeof(KmCDLE));                                // Confirm handling of block by sending DLE
      } else {                                                              // If not STX, it should be valid data block
        #if (KM271_EN_PROTLOG == 1)
        myLog("RX: ");
        for(ii = 0; ii < len; ii++) {
          sprintf(outBuf, "%02X ", data[ii]);
          myLog(outBuf);
        }
        sprintf(outBuf, "%02X %02X %02X\n\r", KM_DLE, KM_ETX, bcc);
        myLog(outBuf);      
        #endif  
        parseInfo(data, len);                                               // Handle data block with event information
        sendTxBlock(KmCDLE, sizeof(KmCDLE));                                // Confirm handling of block by sending DLE
      }
      break;
  }
}

/**
 *  ******************************************************
 * @brief   Sends a single block of data.
 * @details Data is given as "pure" data without protocol bytes.
 *          This function adds the protocol bytes (including BCC calculation) and
 *          sends it to the Ecomatic 2000.
 *          STX and DLE handling needs to be done outside in handleRxBlock().
 * @param   data: Pointer to the data to be send. Single byte data is send right away (i.e,. ptotocol bytes such as STX. DLE, NAK...)
 *                Block data is prepared with doubling DLE and block end indication.
 * @param   len:  Blocksize in number of bytes, without protocol data 
 * @return  none
 * 
 */
static void sendTxBlock(uint8_t *data, int len) {
  uint8_t       buf[256];                                                   // To store bytes to be sent
  int           ii, txLen;
  uint8_t       bcc;
  #if (KM271_EN_PROTLOG == 1)
  char          outBuf[50];                                                 // Just for debugging
  #endif
  
  if(!len) return;                                                          // Nothing to do
  if((len == 1) && ((data[0] == KM_STX) || (data[0] == KM_DLE) || (data[0] == KM_NAK))) {   // Shall a single protocol byte be sent? If yes, send it right away.
    Serial2.write(data, 1);
    #if (KM271_EN_PROTLOG == 1)
    // Some debug logging
    switch(data[0]) {
      case KM_STX:
        myLogLn("TX: STX");
        break;
       case KM_DLE:
        myLogLn("TX: DLE");
        break;
      case KM_NAK:
        myLogLn("TX: NAK");
        break;
    }
    #endif
    return;
  }
  // Here, we need to send a whole block of data. So prepare data.
  for(txLen = 0, ii = 0; ii < len; ii++, txLen++) {
    if(ii) bcc ^= data[ii]; else bcc = data[ii];                            // Initialize / calculate bcc
    buf[txLen] = data[ii];
    if(data[ii] == KM_DLE) {                                                // Doubling of DLE needed?
      bcc ^= data[ii];                                                      // Consider second DLE in BCC calculation
      txLen++;                                                              // Make space for second DLE         
      buf[txLen] = data[ii];                                                // Store second DLE
    }
  }
  // Append buffer with DLE, ETX, BCC
  bcc ^= KM_DLE;
  buf[txLen] = KM_DLE;

  txLen++;                                                                  // Make space for ETX         
  bcc ^= KM_ETX;
  buf[txLen] = KM_ETX;

  txLen++;                                                                  // Make space for BCC
  buf[txLen] = bcc;
  txLen++;
  
  Serial2.write(buf, txLen);                                                // Send the complete block  
  #if (KM271_EN_PROTLOG == 1)
  // Some debug logging
  myLog("TX: ");
  for(ii = 0; ii < txLen; ii++) {
    sprintf(outBuf, "%02X ", buf[ii]);
    myLog(outBuf);
  }
  myLog("\n\r");
  #endif
}

/**
 *  ******************************************************
 * @brief   Interpretation of an received information block
 * @details Checks and handles the information data received.
 *          Handles update of the global s_km271_status and provides event notifications
 *          to other tasks (if requested).
 *          Status data is handles under task lock to ensure consistency of status structure.
 * @param   data: Pointer to the block of data received.
 * @param   len:  Blocksize in number of bytes, without protocol data 
 * @return  none
 */
static void parseInfo(uint8_t *data, int len) {
  int                   ii;
  s_km271_status        tmpState;
  static unsigned int   curEvents = 0;
  unsigned int          newEvents = 0;
  s_km271_event_queue   eventToSend;
  
  #if ((KM271_EN_PARSELOG == 1) || (KM271_EN_PARSE_RESULTLOG == 1))
  char          outBuf[200];                                              // Just for debugging
  #endif

  #if (KM271_EN_PARSELOG == 1)
  // Some debug logging
  myLog("Parsing: ");
  for(ii = 0; ii < len; ii++) {
    sprintf(outBuf, "%02X ", data[ii]);
    myLog(outBuf);
  }
  myLog("\n\r");
  #endif

  // Get current state
  xSemaphoreTake(accessMutex, portMAX_DELAY);                             // Prevent task switch to ensure the whole structure remains constistent
  memcpy(&tmpState, &kmState, sizeof(s_km271_status));
  xSemaphoreGive(accessMutex);
  switch((data[0] * 256) + data[1]) {                                     // Check if we can find known stati
    case 0x8000:                                                          
      tmpState.HeatingCircuitOperatingStates_1 = data[2];                 // 0x8000 : Bitfield
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HeatingCircuitOperatingStates_1 = 0x%02X", tmpState.HeatingCircuitOperatingStates_1);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8001:                                                          
      tmpState.HeatingCircuitOperatingStates_2 = data[2];                 // 0x8001 : Bitfield
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HeatingCircuitOperatingStates_2 = 0x%02X", tmpState.HeatingCircuitOperatingStates_2);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8002:
      tmpState.HeatingForwardTargetTemp = (float)data[2];                 // 0x8002 : Temperature (1C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HeatingForwardTargetTemp = %.1f°C", tmpState.HeatingForwardTargetTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8003:
      tmpState.HeatingForwardActualTemp = (float)data[2];                 // 0x8003 : Temperature (1C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HeatingForwardActualTemp = %.1f°C", tmpState.HeatingForwardActualTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8004:
      tmpState.RoomTargetTemp = decode05cTemp(data[2]);                   // 0x8004 : Temperature (0.5C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "RoomTargetTemp = %.1f°C", tmpState.RoomTargetTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8005:
      tmpState.RoomActualTemp = decode05cTemp(data[2]);                   // 0x8005 : Temperature (0.5C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "RoomActualTemp = %.1f°C", tmpState.RoomActualTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8006:
      tmpState.SwitchOnOptimizationTime = data[2];                        // 0x8006 : Minutes
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "SwitchOnOptimizationTime = %d Minutes", tmpState.SwitchOnOptimizationTime);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8007:  
      tmpState.SwitchOffOptimizationTime = data[2];                       // 0x8007 : Minutes
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "SwitchOffOptimizationTime = %d Minutes", tmpState.SwitchOffOptimizationTime);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8008:  
      tmpState.PumpPower = data[2];                                       // 0x8008 : Percent
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "PumpPower = %d%%", tmpState.PumpPower);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8009:
      tmpState.MixingValue = data[2];                                     // 0x8009 : Percent
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "MixingValue = %d%%", tmpState.MixingValue);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x800c:
      tmpState.HeatingCurvePlus10 = (float)data[2];                       // 0x800c : Temperature (1C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HeatingCurvePlus10 = %.1f°C", tmpState.HeatingCurvePlus10);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x800d:
      tmpState.HeatingCurve0 = (float)data[2];                            // 0x800d : Temperature (1C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HeatingCurve0 = %.1f°C", tmpState.HeatingCurve0);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x800e:
      tmpState.HeatingCurveMinus10 = (float)data[2];                      // 0x800e : Temperature (1C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HeatingCurveMinus10 = %.1f°C", tmpState.HeatingCurveMinus10);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8424:
      tmpState.HotWaterOperatingStates_1 = data[2];                       // 0x8424 : Bitfield
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HotWaterOperatingStates_1 = 0x%02X", tmpState.HotWaterOperatingStates_1);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8425:
      tmpState.HotWaterOperatingStates_2 = data[2];                       // 0x8425 : Bitfield
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HotWaterOperatingStates_2 = 0x%02X", tmpState.HotWaterOperatingStates_2);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8426:
      tmpState.HotWaterTargetTemp = (float)data[2];                       // 0x8426 : Temperature (1C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HotWaterTargetTemp = %.1f°C", tmpState.HotWaterTargetTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8427:
      tmpState.HotWaterActualTemp = (float)data[2];                       // 0x8427 : Temperature (1C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HotWaterActualTemp = %.1f°C", tmpState.HotWaterActualTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8428:
      tmpState.HotWaterOptimizationTime = data[2];                        // 0x8428 : Minutes
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HotWaterOptimizationTime = %d Minutes", tmpState.HotWaterOptimizationTime);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8429:
      tmpState.HotWaterPumpStates = data[2];                              // 0x8429 : Bitfield
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "HotWaterPumpStates = 0x%02X", tmpState.HotWaterPumpStates);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x882a:
      tmpState.BoilerForwardTargetTemp  = (float)data[2];                 // 0x882a : Temperature (1C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "BoilerForwardTargetTemp = %.1f°C", tmpState.BoilerForwardTargetTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x882b:
      tmpState.BoilerForwardActualTemp  = (float)data[2];                 // 0x882b : Temperature (1C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "BoilerForwardActualTemp = %.1f°C", tmpState.BoilerForwardActualTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x882c:
      tmpState.BurnerSwitchOnTemp  = (float)data[2];                      // 0x882c : Temperature (1C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "BurnerSwitchOnTemp = %.1f°C", tmpState.BurnerSwitchOnTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x882d:
      tmpState.BurnerSwitchOffTemp  = (float)data[2];                     // 0x882d : Temperature (1C resolution)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "BurnerSwitchOffTemp = %.1f°C", tmpState.BurnerSwitchOffTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x882e:
      tmpState.BoilerIntegral_1 = data[2];                                // 0x882e : Number (*256)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "BoilerIntegral_1 = %d", tmpState.BoilerIntegral_1);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x882f:
      tmpState.BoilerIntegral_2  = data[2];                               // 0x882f : Number (*1)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "BoilerIntegral_2 = %d", tmpState.BoilerIntegral_2);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8830:
      tmpState.BoilerErrorStates  = data[2];                              // 0x8830 : Bitfield
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "BoilerErrorStates = 0x%02X", tmpState.BoilerErrorStates);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8831:
      tmpState.BoilerOperatingStates = data[2];                           // 0x8831 : Bitfield
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "BoilerOperatingStates = 0x%02X", tmpState.BoilerOperatingStates);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8832:
      tmpState.BurnerStates = data[2];                                    // 0x8832 : Bitfield
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "BurnerStates = 0x%02X", tmpState.BurnerStates);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8833:
      tmpState.ExhaustTemp = (float)data[2];                              // 0x8833 : Temperature (1C resolution)
      if (tmpState.ExhaustTemp > tmpState.MaxExhaustTemp) {               // Track maximum temperature. Reset is caused by rest function.
        tmpState.MaxExhaustTemp = tmpState.ExhaustTemp;
      }
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "ExhaustTemp = %.1f°C", tmpState.ExhaustTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8836:
      tmpState.BurnerOperatingDuration_2 = data[2];                       // 0x8836 : Minutes (*65536)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "BurnerOperatingDuration_2 = %d Minutes", tmpState.BurnerOperatingDuration_2);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8837:
      tmpState.BurnerOperatingDuration_1 = data[2];                       // 0x8837 : Minutes (*256)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "BurnerOperatingDuration_1 = %d Minutes", tmpState.BurnerOperatingDuration_1);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8838:
      tmpState.BurnerOperatingDuration_0 = data[2];                       // 0x8838 : Minutes (*1)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "BurnerOperatingDuration_0 = %d Minutes", tmpState.BurnerOperatingDuration_0);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x893c:
      tmpState.OutsideTemp = decodeNegTemp(data[2]);                      // 0x893c : Temperature (1C resolution, possibly negative)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "OutsideTemp = %.1f°C", tmpState.OutsideTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x893d:
      tmpState.OutsideDampedTemp = decodeNegTemp(data[2]);                // 0x893d : Temperature (1C resolution, possibly negative)
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "OutsideDampedTemp = %.1f°C", tmpState.OutsideDampedTemp);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x893e:
      tmpState.ControllerVersionMain = data[2];                           // 0x893e : Number
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "ControllerVersionMain = %d", tmpState.ControllerVersionMain);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x893f:
      tmpState.ControllerVersionSub = data[2];                            // 0x893f : Number
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "ControllerVersionSub = %d", tmpState.ControllerVersionSub);
      myLogLn(outBuf);
      #endif
      break;
      
    case 0x8940:
      tmpState.Modul = data[2];                                           // 0x8940 : Number
      #if (KM271_EN_PARSE_RESULTLOG == 1)
      sprintf(outBuf, "Modul = %d", tmpState.Modul);
      myLogLn(outBuf);
      #endif
      break;
      
      // Ignore the rest
    default:                                                              // Feel free to decode more, if needed
      break;
  }

  // Prepare events.
  // The most "important" events of the various status information received above are summarized in a single
  // uint32_t event list. This makes it easier to track events (e.g pumps switch on and off again).
  // Whenever one of the collected events has changed, the new event list is send to the receiver
  // on the eventQueue (if the receiver has set up an eventQueue and wants to be notified about event changing).
  newEvents = buildEventList(&tmpState);                                  // Re-generate the event list based on the lates information in tmpState
  tmpState.EventBitList = newEvents;
  // Write current state. Do this only, if something has changed
  xSemaphoreTake(accessMutex, portMAX_DELAY);                             // Prevent task switch to ensure the whole structure remains constistent
  if(memcmp(&tmpState, &kmState, sizeof(s_km271_status))) {
    memcpy(&kmState, &tmpState, sizeof(s_km271_status));
    xSemaphoreGive(accessMutex);    
    // Handle event notification
    // Inform about new events only, if there is a receiver set up and the event list has changed.
    if(curEvents != newEvents) {
      curEvents = newEvents;                                              // Update events
      if(eventQueue) {                                                    // Notify about new events when someone is waiting for notification.
        // Store current event list with timestamp
        eventToSend.uxTimestamp = time(NULL);                      
        eventToSend.eventBitList = curEvents;                
        xQueueSend(eventQueue, &eventToSend, 1000 / portTICK_PERIOD_MS);
      }
    }
  } else {
    xSemaphoreGive(accessMutex);    
  }
}

/**
 *  ******************************************************
 * @brief   Decodes KM271 temperatures with 0.5 C resolution
 * @details The value is provided in 0.5 steps
 * @param   data: the receive dat byte
 * @return  the decoded value as float
 */
static float decode05cTemp(uint8_t data) {
  return ((float)data) / 2.0f;
}


/**
 *  ******************************************************
 * @brief   Decodes KM271 temperatures with negative temperature range
 * @details Values >128 are negative
 * @param   data: the receive dat byte
 * @return  the decoded value as float
 */
static float decodeNegTemp(uint8_t data) {
  if(data > 128) {
        return (((float)(256-data)) * -1.0f);
  } else {
        return (float)data;
  }
}

/**
 *  ******************************************************
 * @brief   Map important events of several status information bytes
 *          into a single event bit list
 * @details 
 * @param   pStatus: Pointer to the status list which shall be used to retrieve current information
 * @return  The collected event list
 */
static uint32_t buildEventList(s_km271_status *pState) {
  uint32_t  ev = 0;
  //************************************************************
  if(pState->HotWaterPumpStates & HotWaterPumpStates_LPUMP) {
    ev |= UPD_EV_LPUMP;
  } else {
    ev &= ~UPD_EV_LPUMP; 
  }
  //************************************************************
  if(pState->HotWaterPumpStates & HotWaterPumpStates_CPUMP) {
    ev |= UPD_EV_CPUMP;
  } else {
    ev &= ~UPD_EV_CPUMP; 
  }
  //************************************************************
  if(pState->BoilerOperatingStates & BoilerOperatingStates_TESTEXH) {
    ev |= UPD_EV_TESTEXH;
  } else {
    ev &= ~UPD_EV_TESTEXH; 
  }
  //************************************************************
  if(pState->PumpPower & PumpPower_VPUMP) {
    ev |= UPD_EV_HPUMP;
  } else {
    ev &= ~UPD_EV_HPUMP; 
  }
  //************************************************************
  if(pState->BurnerStates & BurnerStates_BURNERON) {
    ev |= UPD_EV_BURNERON;
  } else {
    ev &= ~UPD_EV_BURNERON; 
  }
  //************************************************************
  if(pState->BoilerErrorStates & BoilerErrorStates_BRN) {
    ev |= UPD_EV_ERR_BRN;
  } else {
    ev &= ~UPD_EV_ERR_BRN; 
  }
  //************************************************************
  if(pState->BoilerErrorStates & BoilerErrorStates_BSF) {
    ev |= UPD_EV_ERR_BSF;
  } else {
    ev &= ~UPD_EV_ERR_BSF; 
  }
  //************************************************************
  if(pState->BoilerErrorStates & BoilerErrorStates_BRNC) {
    ev |= UPD_EV_ERR_BRNC;
  } else {
    ev &= ~UPD_EV_ERR_BRNC; 
  }
  //************************************************************
  if(pState->BoilerErrorStates & BoilerErrorStates_GASF) {
    ev |= UPD_EV_ERR_EXHF;
  } else {
    ev &= ~UPD_EV_ERR_EXHF; 
  }
  //************************************************************
  if(pState->BoilerErrorStates & BoilerErrorStates_GASL) {
    ev |= UPD_EV_ERR_EXHL;
  } else {
    ev &= ~UPD_EV_ERR_EXHL; 
  }
  //************************************************************
  if(pState->HeatingCircuitOperatingStates_1 & HeatingCircuitOperatingStates1_AUT) {
    ev |= UPD_ST_AUT;
  } else {
    ev &= ~UPD_ST_AUT; 
  }
  //************************************************************
  if(pState->HeatingCircuitOperatingStates_1 & HeatingCircuitOperatingStates1_HWPRIO) {
    ev |= UPD_EV_HWPRIO;
  } else {
    ev &= ~UPD_EV_HWPRIO; 
  }
  //************************************************************
  if(pState->HeatingCircuitOperatingStates_1 & HeatingCircuitOperatingStates1_HOL) {
    ev |= UPD_ST_HOL;
  } else {
    ev &= ~UPD_ST_HOL; 
  }
  //************************************************************
  if(pState->HeatingCircuitOperatingStates_2 & HeatingCircuitOperatingStates2_SUMMER) {
    ev |= UPD_ST_SUMMER;
  } else {
    ev &= ~UPD_ST_SUMMER; 
  }
  //************************************************************
  if(pState->HeatingCircuitOperatingStates_2 & HeatingCircuitOperatingStates2_DAY) {
    ev |= UPD_ST_DAY;
  } else {
    ev &= ~UPD_ST_DAY; 
  }
  //************************************************************
  if(pState->HeatingCircuitOperatingStates_2 & HeatingCircuitOperatingStates2_FB) {
    ev |= UPD_EV_ERR_RC;
  } else {
    ev &= ~UPD_EV_ERR_RC; 
  }
  //************************************************************
  if(pState->HeatingCircuitOperatingStates_2 & HeatingCircuitOperatingStates2_VLF) {
    ev |= UPD_EV_ERR_PT;
  } else {
    ev &= ~UPD_EV_ERR_PT; 
  }
  //************************************************************
  if(pState->HotWaterOperatingStates_1 & HotWaterOperatingStates1_DIF) {
    ev |= UPD_EV_DIF;
  } else {
    ev &= ~UPD_EV_DIF; 
  }
  //************************************************************
  if(pState->HotWaterOperatingStates_1 & HotWaterOperatingStates1_RLOADING) {
    ev |= UPD_EV_RLOADING;
  } else {
    ev &= ~UPD_EV_RLOADING; 
  }
  //************************************************************
  if(pState->HotWaterOperatingStates_1 & HotWaterOperatingStates1_EDIF) {
    ev |= UPD_EV_ERR_EDIF;
  } else {
    ev &= ~UPD_EV_ERR_EDIF; 
  }
  //************************************************************
  if(pState->HotWaterOperatingStates_1 & HotWaterOperatingStates1_HWF) {
    ev |= UPD_EV_ERR_HWF;
  } else {
    ev &= ~UPD_EV_ERR_HWF; 
  }
  //************************************************************
  if(pState->HotWaterOperatingStates_1 & HotWaterOperatingStates1_HWC) {
    ev |= UPD_EV_ERR_HWC;
  } else {
    ev &= ~UPD_EV_ERR_HWC; 
  }
  //************************************************************
  if(pState->HotWaterOperatingStates_1 & HotWaterOperatingStates1_AND) {
    ev |= UPD_EV_ERR_AND;
  } else {
    ev &= ~UPD_EV_ERR_AND; 
  }
  //************************************************************
  if(pState->HotWaterOperatingStates_2 & HotWaterOperatingStates2_LOADING) {
    ev |= UPD_EV_LOADING;
  } else {
    ev &= ~UPD_EV_LOADING; 
  }
  //************************************************************
  if(pState->HotWaterOperatingStates_2 & HotWaterOperatingStates2_DAY) {
    ev |= UPD_ST_WW_ON;
  } else {
    ev &= ~UPD_ST_WW_ON; 
  }
  //************************************************************
  return ev;
}

/**
 *  ******************************************************
 * @brief   Maps logging to the desired streams.
 * @details This gives a bit more flexibility to logging.
 *          Currently, logging is provided to Serial and TelnetStream.
 *          This can definitively solved more elegant, but this way ist works...
 * @param   txt: The string to log
 * @return  none
 */
static void myLog(const char *txt) {
  Serial.print(txt);
  TelnetStream.print(txt);
}
static void myLogLn(const char *txt) {
  Serial.println(txt);
  TelnetStream.println(txt);
}
