//*****************************************************************************
//
// File Name  : 'km271_prot.h'
// Title      : Handles 3964 protocol for KM271
// Author     : Michael Meyer
// Created    : 08.01.2022
// Version    : 0.1
// Target MCU : ESP32/Arduino
// Indicator  : km
//
//
//*****************************************************************************

#pragma once
#ifdef __cplusplus
extern "C" {
#endif

#include "time.h"

//*****************************************************************************
// Defines
//*****************************************************************************

//*****************************************************************************
// Structures
//*****************************************************************************

// Bitfiled definitions for bitfields of values of the s_km271_status struct.
// This is just a selection. There is more to specify if needed.
// We are not definig this as bitfield to ensure portability...

// For HotWaterPumpStates : 0x8429 : Bitfield
#define HotWaterPumpStates_LPUMP              (1<<0)                // Hot water load pump
#define HotWaterPumpStates_CPUMP              (1<<1)                // Hot water circulation pump

// For BoilerOperatingStates : 0x8831 : Bitfield
#define BoilerOperatingStates_TESTEXH         (1<<0)                // Exhaust test

// For PumpPower : 0x8008 : Percent
#define PumpPower_VPUMP                       (1<<5)                // Heat pump is on (bit 5 is set when 100%) 

// For BurnerStates : 0x8832 : Bitfield
#define BurnerStates_BURNERON                 (1<<0)                // Burner ON

// For BoilerErrorStates : 0x8830 : Bitfield
#define BoilerErrorStates_BRN                 (1<<0)                // Error burner
#define BoilerErrorStates_BSF                 (1<<1)                // Error boiler sensor
#define BoilerErrorStates_BRNC                (1<<3)                // Boiler remains cold
#define BoilerErrorStates_GASF                (1<<4)                // Error exhaust sensor
#define BoilerErrorStates_GASL                (1<<5)                // Exhaust temperatur obove limit

// For HeatingCircuitOperatingStates_1 : 0x8000 : Bitfield
#define HeatingCircuitOperatingStates1_AUT    (1<<2)                // Automatic operation
#define HeatingCircuitOperatingStates1_HWPRIO (1<<3)                // Priority to hot water 
#define HeatingCircuitOperatingStates1_HOL    (1<<5)                // Holiday operation

// For HeatingCircuitOperatingStates_2 : 0x8001 : Bitfield
#define HeatingCircuitOperatingStates2_SUMMER (1<<0)                // Summer operation
#define HeatingCircuitOperatingStates2_DAY    (1<<1)                // Day operation
#define HeatingCircuitOperatingStates2_FB     (1<<2)                // No communication with remote control
#define HeatingCircuitOperatingStates2_VLF    (1<<3)                // Problem sensor forward circuit

// For HotWaterOperatingStates_1 :0x8424 : Bitfield
#define HotWaterOperatingStates1_AUTO         (1<<0)                // Automatic operation
#define HotWaterOperatingStates1_DIF          (1<<1)                // Desinfection ongoing
#define HotWaterOperatingStates1_RLOADING     (1<<2)                // Re-load ongoing
#define HotWaterOperatingStates1_EDIF         (1<<4)                // Error desinfection
#define HotWaterOperatingStates1_HWF          (1<<5)                // Error hot water sensor
#define HotWaterOperatingStates1_HWC          (1<<6)                // Hot water remains cold
#define HotWaterOperatingStates1_AND          (1<<7)                // Error anode

// For HotWaterOperatingStates_2 : 0x8425 : Bitfield
#define HotWaterOperatingStates2_LOADING      (1<<0)                // Hot water loading
#define HotWaterOperatingStates2_DAY          (1<<5)                // Hot water day operation

//********************************************************************************************

// Event bits for s_km271_status.EventBitList
#define UPD_ST_SUMMER                         (((uint32_t)1)<<0)    // 1 Summer operation, 0 regular operation
#define UPD_ST_DAY                            (((uint32_t)1)<<1)    // 1 Day operation, 0 night operation
#define UPD_ST_HOL                            (((uint32_t)1)<<2)    // 1 Holiday operation, 0 regular operation
#define UPD_ST_AUT                            (((uint32_t)1)<<3)    // 1 Automatic operation, 0 manual operation
#define UPD_EV_BURNERON                       (((uint32_t)1)<<4)    // 1 Burner running
#define UPD_EV_HPUMP                          (((uint32_t)1)<<5)    // 1 Heat pump is running
#define UPD_EV_LPUMP                          (((uint32_t)1)<<6)    // 1 Ladepumpe is running
#define UPD_EV_CPUMP                          (((uint32_t)1)<<7)    // 1 circulation pump is running
#define UPD_EV_HWPRIO                         (((uint32_t)1)<<8)    // 1 Hot water priority
#define UPD_EV_LOADING                        (((uint32_t)1)<<9)    // 1 Hot water is loading
#define UPD_EV_RLOADING                       (((uint32_t)1)<<10)   // 1 Hot water re-loading
#define UPD_EV_TESTEXH                        (((uint32_t)1)<<11)   // 1 Exhaust test ongoing
#define UPD_EV_DIF                            (((uint32_t)1)<<12)   // 1 Desinfection ongoing
#define UPD_EV_ERR_RC                         (((uint32_t)1)<<13)   // 1 Error: remote control
#define UPD_EV_ERR_PT                         (((uint32_t)1)<<14)   // 1 Error: Primary circuit temperature sensor
#define UPD_EV_ERR_EDIF                       (((uint32_t)1)<<15)   // 1 Error: Desinfection
#define UPD_EV_ERR_HWF                        (((uint32_t)1)<<16)   // 1 Error: Hot water temperatur sensor
#define UPD_EV_ERR_HWC                        (((uint32_t)1)<<17)   // 1 Error: Hot water remains cold
#define UPD_EV_ERR_AND                        (((uint32_t)1)<<18)   // 1 Error: Anode defect
#define UPD_EV_ERR_BRN                        (((uint32_t)1)<<19)   // 1 Error: Burner error
#define UPD_EV_ERR_BRNC                       (((uint32_t)1)<<20)   // 1 Error: Boiler remains cold
#define UPD_EV_ERR_BSF                        (((uint32_t)1)<<21)   // 1 Error: Boiler sensor
#define UPD_EV_ERR_EXHF                       (((uint32_t)1)<<22)   // 1 Error: Exhaust temperatur sensor
#define UPD_EV_ERR_EXHL                       (((uint32_t)1)<<23)   // 1 Error: Exhaust too hot
#define UPD_ST_WW_ON                          (((uint32_t)1)<<24)   // 1 Hot water preparation on

//********************************************************************************************

// This struicure contains all values read from the heating controller.
// This structure is kept up-to-date automatically by the km27_prot.cpp.
// Use km271GetStatus() to get the most recent copy of these values in a thread-safe manner.
typedef struct {
  // Retrieved values
  uint8_t   HeatingCircuitOperatingStates_1;                        // 0x8000 : Bitfield
  uint8_t   HeatingCircuitOperatingStates_2;                        // 0x8001 : Bitfield
  float     HeatingForwardTargetTemp;                               // 0x8002 : Temperature (1C resolution)
  float     HeatingForwardActualTemp;                               // 0x8003 : Temperature (1C resolution)
  float     RoomTargetTemp;                                         // 0x8004 : Temperature (0.5C resolution)
  float     RoomActualTemp;                                         // 0x8005 : Temperature (0.5C resolution)
  uint8_t   SwitchOnOptimizationTime;                               // 0x8006 : Minutes
  uint8_t   SwitchOffOptimizationTime;                              // 0x8007 : Minutes
  uint8_t   PumpPower;                                              // 0x8008 : Percent
  uint8_t   MixingValue;                                            // 0x8009 : Percent
  float     HeatingCurvePlus10;                                     // 0x800c : Temperature (1C resolution)
  float     HeatingCurve0;                                          // 0x800d : Temperature (1C resolution)
  float     HeatingCurveMinus10;                                    // 0x800e : Temperature (1C resolution)
  uint8_t   HotWaterOperatingStates_1;                              // 0x8424 : Bitfield
  uint8_t   HotWaterOperatingStates_2;                              // 0x8425 : Bitfield
  float     HotWaterTargetTemp;                                     // 0x8426 : Temperature (1C resolution)
  float     HotWaterActualTemp;                                     // 0x8427 : Temperature (1C resolution)
  uint8_t   HotWaterOptimizationTime;                               // 0x8428 : Minutes
  uint8_t   HotWaterPumpStates;                                     // 0x8429 : Bitfield
  float     BoilerForwardTargetTemp;                                // 0x882a : Temperature (1C resolution)
  float     BoilerForwardActualTemp;                                // 0x882b : Temperature (1C resolution)
  float     BurnerSwitchOnTemp;                                     // 0x882c : Temperature (1C resolution)
  float     BurnerSwitchOffTemp;                                    // 0x882d : Temperature (1C resolution)
  uint8_t   BoilerIntegral_1;                                       // 0x882e : Number (*256)
  uint8_t   BoilerIntegral_2;                                       // 0x882f : Number (*1)
  uint8_t   BoilerErrorStates;                                      // 0x8830 : Bitfield
  uint8_t   BoilerOperatingStates;                                  // 0x8831 : Bitfield
  uint8_t   BurnerStates;                                           // 0x8832 : Bitfield
  float     ExhaustTemp;                                            // 0x8833 : Temperature (1C resolution)
  uint8_t   BurnerOperatingDuration_2;                              // 0x8836 : Minutes (*65536)
  uint8_t   BurnerOperatingDuration_1;                              // 0x8837 : Minutes (*256)
  uint8_t   BurnerOperatingDuration_0;                              // 0x8838 : Minutes (*1)
  float     OutsideTemp;                                            // 0x893c : Temperature (1C resolution, possibly negative)
  float     OutsideDampedTemp;                                      // 0x893e : Temperature (1C resolution, possibly negative)
  uint8_t   ControllerVersionMain;                                  // 0x893e : Number
  uint8_t   ControllerVersionSub;                                   // 0x893f : Number
  uint8_t   Modul;                                                  // 0x8940 : Number

  // Calculated values
  float     MaxExhaustTemp;                                         // Collected max value from midnight to midnight (one day) : Temperature (1C resolution)
  uint32_t  EventBitList;                                           // Summarized events generated from information of above. Bitfield.
} s_km271_status;

typedef struct {
  time_t  uxTimestamp;                                              // Unix timestamp when the event was happening               
  uint32_t  eventBitList;                                           // The bitlist with the events        
} s_km271_event_queue;

// Return values used by the KM271 protocol functions
typedef enum {
  RET_OK = 0,
  RET_ERR,
} e_ret;

//*****************************************************************************
// Variables exported
//*****************************************************************************
extern TaskHandle_t                hKmRxTaskHandle;               // To store tge handle of the RX task
//*****************************************************************************
// Function prototypes
//*****************************************************************************
e_ret km271ProtInit(int rxPin, int txPin, QueueHandle_t eventQ);  // Initializes the KM271 communication. To be called once by setup().
void  km271GetStatus(s_km271_status *pDestStatus);                // Retrieves the current status
void km271ResetDailyValues();                                     // Reset values which are tracked on a daily basis

#ifdef __cplusplus
}
#endif
