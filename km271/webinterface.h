//*****************************************************************************
//
// File Name  : 'webinterface.h'
// Title      : Handles the webinterfaces for homeautomation M.Meyer.
//              Just to be seen as example. This is highly specific for a specific, already
//              existing infrastructure.
// Author     : Michael Meyer
// Created    : 20.01.2022
// Version    : 0.1
// Target MCU : ESP32/Arduino
// Indicator  : wi
//
//
//*****************************************************************************

#pragma once
#ifdef __cplusplus
extern "C" {
#endif

//*****************************************************************************
// Defines
//*****************************************************************************

//*****************************************************************************
// Structures
//*****************************************************************************

//*****************************************************************************
// Variables exported
//*****************************************************************************
// For debugging, e.g. check available stack
extern TaskHandle_t          hWebStatusTaskHandle;                                   // To store the web status request task handle
extern TaskHandle_t          hWebUpdateTaskHandle;                                   // To store the web update task handle
extern TaskHandle_t          hWebEventTaskHandle;                                    // To store the web event request task handle

//*****************************************************************************
// Function prototypes
//*****************************************************************************
e_ret webInterfaceInit(const char *ssid, const char * password, QueueHandle_t *eventQ);   // Initializes the web interface. To be called once by setup().

#ifdef __cplusplus
}
#endif
